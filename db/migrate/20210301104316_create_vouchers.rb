class CreateVouchers < ActiveRecord::Migration[5.2]
  def change
    create_table :vouchers do |t|
      t.string :code
      t.string :type
      t.references :store, foreign_key: true

      t.timestamps
    end
  end
end
