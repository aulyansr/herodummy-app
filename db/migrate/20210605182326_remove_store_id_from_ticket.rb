class RemoveStoreIdFromTicket < ActiveRecord::Migration[5.2]
  def change
    remove_column :tickets, :store_id, :string
  end
end
