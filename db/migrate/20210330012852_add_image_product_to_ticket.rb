class AddImageProductToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :image_product, :string
  end
end
