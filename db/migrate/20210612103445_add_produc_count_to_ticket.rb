class AddProducCountToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :product_count, :integer
  end
end
