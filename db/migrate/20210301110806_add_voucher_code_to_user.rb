class AddVoucherCodeToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :voucher_code, :string
  end
end
