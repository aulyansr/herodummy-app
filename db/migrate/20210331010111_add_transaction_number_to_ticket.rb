class AddTransactionNumberToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :transaction_number, :string
  end
end
