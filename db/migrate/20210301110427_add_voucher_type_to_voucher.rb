class AddVoucherTypeToVoucher < ActiveRecord::Migration[5.2]
  def change
    add_column :vouchers, :voucher_type, :string
  end
end
