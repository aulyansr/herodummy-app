class AddCodeToStore < ActiveRecord::Migration[5.2]
  def change
    add_column :stores, :code, :string
  end
end
