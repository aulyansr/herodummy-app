class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.references :user, foreign_key: true
      t.references :voucher, foreign_key: true
      t.string :proff_of_payment

      t.timestamps
    end
  end
end
