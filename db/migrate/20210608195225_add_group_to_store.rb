class AddGroupToStore < ActiveRecord::Migration[5.2]
  def change
    add_column :stores, :group, :string
  end
end
