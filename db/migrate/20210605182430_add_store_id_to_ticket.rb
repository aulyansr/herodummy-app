class AddStoreIdToTicket < ActiveRecord::Migration[5.2]
  def change
    add_reference :tickets, :store, foreign_key: true
  end
end
