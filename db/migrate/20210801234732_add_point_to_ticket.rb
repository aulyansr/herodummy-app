class AddPointToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :point, :integer
  end
end
