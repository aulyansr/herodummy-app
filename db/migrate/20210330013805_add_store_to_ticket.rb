class AddStoreToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :store_id, :integer
  end
end
