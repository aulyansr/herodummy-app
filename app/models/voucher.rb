class Voucher < ApplicationRecord
  belongs_to :store

    validates :code, uniqueness: true, presence: true
end
