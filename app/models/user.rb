class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :tickets
  has_one :store

    validates :email, uniqueness: true, presence: true
    validates :ktp, presence: true
    validates :phone_number, length: {minimum: 6, maximum: 15}, uniqueness: true, allow_blank: true
    before_validation :normalize_phone_number

  mount_uploader :ktp, ImageUploader

  Roles = {
    admin: 'ADM',
    customer: 'CRM',
    store: "STR",
  }.freeze

  def is_admin?
    self.role == Roles[:admin]
  end

  def is_customer?
    self.role == Roles[:customer]
  end

  def is_store?
    self.role == Roles[:store]
  end

  def normalize_phone_number
    if self.phone_number.present?
      number = self.phone_number.scan(/\d/).join('')

      if number[0..1] == '08'
        self.phone_number = "#{number}"
        prefix  = self.phone_number
      elsif number[0..2] == '+62'
        self.phone_number = "0#{number[3..number.length]}"
        prefix  = self.phone_number
      elsif number[0..1] == '62'
        self.phone_number = "0#{number[2..number.length]}"
        prefix = self.phone_number
      else
        self.phone_number = "08#{number}"
        prefix  = self.phone_number
      end

      prefix  = prefix.scan(/\d/).join('')

      if prefix[0..3] == ["0831","0832","0832","0838"]
        self.provider = "AX10"
      elsif ["0856","0857","0858","0815","0816","0855","0814"].include?(prefix[0..3])
        self.provider = "I10"
      elsif  ["0881","0882","0883","0884","0885","0886","0887","0888","0889"].include?(prefix[0..3])
        self.provider = "SM10"
      elsif  ["0811","0812","0813","0821","0822","0823","0852","0853","0851"].include?(prefix[0..3])
        self.provider = "S10"
      elsif ["0896","0897","0898","0899","0895"].include?(prefix[0..3])
        self.provider = "T10"
      elsif ["0817","0818","0819","0859","0877","0878"].include?(prefix[0..3])
        self.provider = "X10"
      else
        self.provider = "UNKNOWN"
      end
    end
  end

  def self.send_pulsa(code, phoneNumber, ticket)
    url = "https://tripay.co.id/api/v2/transaksi/pembelian"
    @send_pulsa = Faraday.post('https://tripay.co.id/api/v2/transaksi/pembelian') do |req|
      req.headers['Authorization'] = "Bearer XffCVv829TfJU3jm0RSZg6k81w2C4ymK"
      req.headers['Content-Type'] = 'application/json'
      req.body = { inquiry: 'I', code: code, phone: phoneNumber, pin: '2156', api_trxid:  ticket}.to_json
    end
    logger.info @send_pulsa.body
  end

  def self.send_notif(user)
    mg_client = Mailgun::Client.new 'd95979082ed1a94540dc0ba29d3a3d88-2a9a428a-0451ae4f'

    # Define your message parameters
    message_params =  { from: 'no-reply@siapaktiflagi.com',
                        to:   "#{user.email}",
                        subject: 'Selamat Bergabung di Program #siapaktiflagi',
                        template: 'thanks_message',
                        "v:name":"#{user.name}"
                      }

    # Send your message through the client
    mg_client.send_message 'siapaktiflagi.com', message_params
  end

  def self.notif_admin
    mg_client = Mailgun::Client.new 'd95979082ed1a94540dc0ba29d3a3d88-2a9a428a-0451ae4f'

    # Define your message parameters
    message_params =  { from: 'no-reply@siapaktiflagi.com',
                        to:   "project@seva-advertising.com",
                        subject: 'Ada Poin Baru masuk',
                        template: 'notif'
                      }

    # Send your message through the client
    mg_client.send_message 'siapaktiflagi.com', message_params
  end

  def self.notif_approve(user)
    mg_client = Mailgun::Client.new 'd95979082ed1a94540dc0ba29d3a3d88-2a9a428a-0451ae4f'

    # Define your message parameters
    message_params =  { from: 'no-reply@siapaktiflagi.com',
                        to:   "#{user.email}",
                        subject: 'Pulsa Sudah dikirimkan',
                        template: 'success',
                        "v:name":"#{user.name}",
                        "v:phone_number":"#{user.phone_number}"
                      }

    # Send your message through the client
    mg_client.send_message 'siapaktiflagi.com', message_params
  end



end
