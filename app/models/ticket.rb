class Ticket < ApplicationRecord
  belongs_to :user
  belongs_to :store

  validates :proff_of_payment,  presence: true
  validates :image_product,  presence: true

  mount_uploader :proff_of_payment, ImageUploader
  mount_uploader :image_product, ImageUploader

end
