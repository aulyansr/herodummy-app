class NotifMailer < ApplicationMailer

  default from: "Ifree - Siap Aktif Lagi <no-reply@siapaktiflagi.com>"

  def notif(ticket)
   @ticket = ticket
   mail( :to => [ticket.user.email],
     :subject => "Poin Kamu Bertambah",
     :display_name => "Notifikasi - Ifree Siap Aktif Lagi" )
  end


end
