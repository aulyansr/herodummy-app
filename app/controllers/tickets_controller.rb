class TicketsController < ApplicationController
  before_action :set_ticket, only: %i[ show edit update destroy ]
  before_action :authenticate_user!
  layout "admin", :only => [:index, :update, :edit, :detail]
  # GET /tickets or /tickets.json
  def index
    @tickets = Ticket.all

    if current_user.is_customer?
      redirect_to root_path
    end

  end

  # GET /tickets/1 or /tickets/1.json
  def show
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
    if params[:voucher].present?
      store = Store.where(code: params[:voucher]).first
    else
      store = Store.where(code: session[:vcrcode]).first
    end

  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets or /tickets.json
  def create

    @ticket = Ticket.new(ticket_params)

    if params[:voucher].present?
      store = Store.where(code: params[:voucher]).first
    else
      store = Store.where(code: session[:vcrcode]).first
    end


    if store.present?
      @ticket.store_id = store.id
    end
    @ticket.user_id = current_user.id
      respond_to do |format|
        if @ticket.save
          User.notif_admin
          User.send_notif(@ticket.user)
          format.html { redirect_to @ticket, notice: "Ticket was successfully created." }
          format.json { render :show, status: :created, location: @ticket }
        else
          format.html { redirect_to new_ticket_path, notice: "Ticket was successfully updated." }
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      end


  end

  # PATCH/PUT /tickets/1 or /tickets/1.json
  def update





      if @ticket.update(ticket_params)


          redirect_to tickets_path, notice: "Ticket was successfully updated."





      else
      render :edit, status: :unprocessable_entity

    end

  end

  # DELETE /tickets/1 or /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: "Ticket was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def detail
    @ticket = Ticket.find(params[:id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def ticket_params
      params.require(:ticket).permit(:user_id, :voucher_id, :proff_of_payment, :store_id, :status, :image_product, :transaction_number, :reason, :product_count, :point)
    end
end
