class ApplicationController < ActionController::Base

  before_action :configure_permitted_parameters, if: :devise_controller?
  def after_sign_up_path_for(resource_or_scope)
      new_ticket_path
  end

  def menu_type
    @menu_homepage = false
  end


  protected

  def after_sign_out_path_for(resource)
    root_path
  end

  def after_sign_in_path_for(resource)
    if current_user.is_admin?
      dashboard_path
    elsif current_user.is_store?
      store_profile_path
    elsif session[:vcrcode].present?
      new_ticket_path
    elsif session[:group].present?
      new_ticket_path(group: session[:group])  
    else
      profile_path
    end

  end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:email, :password, :phone_number, :address, :voucher_code, :role, :ktp])
    end

end
