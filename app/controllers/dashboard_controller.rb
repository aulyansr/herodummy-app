class DashboardController < ApplicationController
  before_action :authenticate_user!
  layout "admin"

  def index
    @tickets = Ticket.where(status: ["new","onprogress"])
    @stores = Store.all
    @customers = User.where(role: "CRM")
  end
end
