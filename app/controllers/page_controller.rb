class PageController < ApplicationController
  def index
    @menu_homepage = true
  end

  def rdr
    vcr = Store.where(code: params[:voucher_code]).last
    xvcr = Store.where(code: params[:voucher_code].upcase).last

    if user_signed_in?

      if vcr.present?
        redirect_to new_ticket_path(voucher: params[:voucher_code])
      elsif xvcr.present?
        redirect_to new_ticket_path(voucher: params[:voucher_code].upcase)
      end

    else
      if vcr.present?
        redirect_to new_registration_path(User, voucher: params[:voucher_code])
      elsif xvcr.present?
        redirect_to new_registration_path(User, voucher: params[:voucher_code].upcase)
      end
    end



  end

  def profile
    # code
  end

  def str_profile
    # code
  end
  def tnc
  end
  def privacy_policy
    # code
  end
  def howto
    # code
  end

  def all_store
    # code
  end

  def store_leaderboards
    # code
  end
  def user_profile
    @user = User.find(params[:id])
  end
end
