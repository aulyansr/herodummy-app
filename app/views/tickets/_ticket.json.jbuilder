json.extract! ticket, :id, :user_id, :voucher_id, :proff_of_payment, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
