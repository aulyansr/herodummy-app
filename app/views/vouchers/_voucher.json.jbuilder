json.extract! voucher, :id, :code, :voucher_type, :store_id, :created_at, :updated_at
json.url voucher_url(voucher, format: :json)
