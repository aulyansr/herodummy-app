json.data(@stores) do |x|

  json.name "#{x.name}"
  json.city x.city
  json.address x.address
  json.code x.code
  json.url_name "www.siapaktiflagi.com/#{x.code}"
  json.qr_code "URL:https://siapaktiflagi.com/#{x.code}"

end
