Rails.application.routes.draw do


  root to: 'page#index'

  resources :stores  do
    collection { post :import }
  end
  resources :tickets
  resources :vouchers
  devise_for :users
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'dashboard' => "dashboard#index"
  get '/vouchers', to: 'vouchers#index', as: :vouchers_path
  get '/profile', to: 'page#profile', as: :profile
  get '/store-profile', to: 'page#store_profile', as: :store_profile
  get '/user-profile', to: 'page#user_profile', as: :user_profile
  get '/term-and-condition', to: 'page#tnc', as: :tnc
  get '/privacy-policy', to: 'page#privacy_policy', as: :privacy
  get '/how-to-join', to: 'page#howto', as: :howto
  get '/all-store', to: 'page#all_store', as: :all_store
  get '/store-leaderboards', to: 'page#store_leaderboards', as: :store_leaderboards
  get ':voucher_code', to: 'page#rdr', as: :rdr
  get '/detail/:id', to: 'tickets#detail', as: :dtl
end
